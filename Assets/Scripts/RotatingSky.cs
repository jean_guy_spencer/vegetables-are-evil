﻿using UnityEngine;
using System.Collections;

public class RotatingSky : MonoBehaviour {
    Vector3 rotation;

	// Use this for initialization
	void Start () {
        rotation = Vector3.zero;
	}
	
	// Update is called once per frame
	void Update () {
        rotation.y = Time.deltaTime;
        transform.Rotate(rotation);
	}
}
