﻿using UnityEngine;
using System.Collections;

public enum DIRECTION
{ 
    LEFT, 
    RIGHT, 
    MAX 
}

public enum GAMESTATE
{
    TITLE,
    GAME,
    ENDGAME,
    SCARE,
    HOWTO,
    GAMEOVER,
    WIN,
    CAKE,
    BOSS,
    MAX
}