﻿using UnityEngine;
using System.Collections;

public class CakeSpin : MonoBehaviour 
{
    Vector3 rotation;

    void Start()
    {
        rotation = Vector3.zero;
        rotation.x = 0;
        rotation.z = 0;
    }
	
	void Update () 
    {
        rotation.y = Time.deltaTime * 20;
        transform.Rotate(rotation);
	}
}
