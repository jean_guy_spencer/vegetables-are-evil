﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class HowToPlay : MonoBehaviour
{
    [System.Serializable]
    public class TutorialScreen
    {
        public Texture2D image;
        public string text;
    }

    public List<TutorialScreen> screens;
    public Game game;
    public GUISkin skin;

    private Rect imageRect;
    private Rect textRect;
    private Rect nextRect;
    private Rect prevRect;
    private int currentScreen;

    void Start()
    {
        currentScreen = 0;
        imageRect = new Rect(Screen.width / 5, 0, (Screen.width / 5) * 3, (Screen.height / 4) * 2);
        textRect = new Rect(Screen.width / 5, (Screen.height / 4) * 2, (Screen.width / 5) * 3, (Screen.height / 4) * 2);
        prevRect = new Rect(0, 0, Screen.width / 5, Screen.height);
        nextRect = new Rect((Screen.width / 5) * 4, 0, Screen.width / 5, Screen.height);
    }

    void Update()
    {
        if (game.state == GAMESTATE.HOWTO)
        {
            if (Input.GetKey(KeyCode.Escape))
                currentScreen--;
        }

        if (currentScreen < 0 || currentScreen > screens.Count-1)
        {
            game.state = GAMESTATE.TITLE;
            currentScreen = 0;
        }
    }

    void OnGUI()
    {
        GUI.skin = skin;
        GUI.skin.button.fontSize = Screen.height / 10;
        GUI.skin.label.fontSize = Screen.height / 15;

        if (game.state == GAMESTATE.HOWTO)
        {
            GUI.Label(imageRect, screens[currentScreen].image);
            GUI.Label(textRect, screens[currentScreen].text);

            if (GUI.Button(prevRect, "<"))
                currentScreen--;

            if (GUI.Button(nextRect, ">"))
                currentScreen++;
        }
    }
}
