﻿using UnityEngine;
using System.Collections;

public class ObjectToHit : MonoBehaviour 
{
    public DIRECTION direction { get; private set; }
    public bool isVeggie;
    public void Hit() { }

    public Vector3 dir { get; private set; }

    private float movementSpeed;
    
    void Start() 
    {
        this.gameObject.AddComponent<RotatingObject>();
        movementSpeed = 0.5f;
	}

    public void SetDir(DIRECTION spawnDirection)
    {
        direction = spawnDirection;

        switch (direction)
        {
            case DIRECTION.LEFT:
                dir = new Vector3(-1, 0, 0);
                break;

            case DIRECTION.RIGHT:
                dir = new Vector3(1, 0, 0);
                break;
        }
    }

	void Update () 
    {
        transform.position += dir * Time.deltaTime * movementSpeed;

        switch (direction)
        {
            case DIRECTION.LEFT:
                if (transform.position.x < -1.5f)
                {
                    Destroy();
                }
                    break;

            case DIRECTION.RIGHT:
                if (transform.position.x > 1.5f)
                    Destroy();
                break;
        }
    }

    void OnMouseDown()
    {
        Camera.main.GetComponent<Game>().Hit(this.gameObject);
    }

    void Destroy()
    {
        GameObject.Find("Main Camera").GetComponent<Game>().currentObjects.Remove(this.gameObject);
        Destroy(this.gameObject);
    }
}
