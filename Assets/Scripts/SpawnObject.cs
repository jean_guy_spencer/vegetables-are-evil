﻿using UnityEngine;
using System.Collections;

public class SpawnObject : MonoBehaviour
{
    public Game game;
    public DIRECTION spawnDirection;
    private Vector3 dir;
    private float time;

    void Start()
    {
        time = 0.0f;
    }

    void Update()
    {
        if (game.state == GAMESTATE.GAME)
        {
            time += Time.deltaTime;

            if (time > game.spawnInterval)
            {
                time = 0.0f;
                Spawn();
            }
        }
    }

    public void Spawn()
    {
        GameObject[] tempList;

        if ((Random.Range(0, 100)) > game.chanceOfVeggie)
            tempList = game.goodStuff;

        else
            tempList = game.veggies;

        GameObject newObject = Instantiate(tempList[Random.Range(0, tempList.Length)], transform.position, Quaternion.identity) as GameObject;
        newObject.GetComponent<ObjectToHit>().SetDir(spawnDirection);
        game.currentObjects.Add(newObject);
    }
}