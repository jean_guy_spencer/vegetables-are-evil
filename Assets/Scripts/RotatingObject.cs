﻿using UnityEngine;
using System.Collections;

public class RotatingObject : MonoBehaviour 
{
    Vector3 rotation;

    void Start()
    {
        rotation = Vector3.zero;
    }

    void Update()
    {
        rotation.x = Time.deltaTime*-50;
        rotation.y = Time.deltaTime*50;
        rotation.z = Time.deltaTime*-50;
        transform.Rotate(rotation);
    }
}
