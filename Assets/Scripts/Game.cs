﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class Game : MonoBehaviour
{
    public GameObject SuccessParticle;
    public GameObject FailParticle;
    public GameObject[] veggies;
    public GameObject[] goodStuff;
    public GameObject boss;
    public GameObject cake;
    public int score;
    public int chanceOfVeggie;
    public float gravity;
    public float spawnInterval;
    public Texture2D titleScreen;
    public Texture2D scare;
    public GUISkin defaultSkin;
    public List<GameObject> currentObjects;
    public GAMESTATE state;
    public float maxTime;
    public int goal;
    public bool showScore;

    private Rect lowerLeftButton;
    private Rect lowerRightButton;
    private Rect scoreRect;
    private Rect timeRect;
    private Rect titleRect;
    private Rect lowerMiddleButton;

    private float time;
    private float endTimer;
    private int mistakes;

    void Start()
    {
        cake.SetActive(false);
        cake = Instantiate(cake, cake.transform.position, cake.transform.rotation) as GameObject;
        state = GAMESTATE.TITLE;
        Physics.gravity = new Vector3(0, gravity, 0);
        currentObjects = new List<GameObject>();

        // Rectangles for labels and buttons
        titleRect = new Rect(0, 0, Screen.width, Screen.height);
        lowerLeftButton = new Rect(0, (Screen.height / 4) * 3, Screen.width / 2, Screen.height / 4);
        lowerRightButton = new Rect(Screen.width / 2, (Screen.height / 4) * 3, Screen.width / 2, Screen.height / 4);
        lowerMiddleButton = new Rect(Screen.width / 4, (Screen.height / 4) * 3, Screen.width / 2, Screen.height / 4);
        scoreRect = new Rect(0, 0, Screen.width / 3, Screen.height / 5);
        timeRect = new Rect((Screen.width / 3) * 2, 0, Screen.width / 3, Screen.height / 5);
    }

    void Update()
    {
        switch (state)
        {
            case GAMESTATE.TITLE:
                if (Input.GetKey(KeyCode.Escape))
                {
                    Application.Quit();
                }
                break;

            case GAMESTATE.GAME:
                time -= Time.deltaTime;
                if (time <= 0.0f)
                {
                    DestroyAll();
                    state = GAMESTATE.ENDGAME;
                }
                break;

            case GAMESTATE.ENDGAME:
                if (mistakes >= 3)
                        EndGame();

                else
                {
                    endTimer -= Time.deltaTime;
                    if (endTimer < 0)
                        EndGame();
                }
                break;
        }
    }

    public void EndGame()
    {
        if (score < goal)
            state = GAMESTATE.GAMEOVER;
        else
            state = GAMESTATE.WIN;
    }

    public void DestroyAll()
    {
        foreach (GameObject g in currentObjects)
            Destroy(g);

        currentObjects.Clear();
    }

    public void Hit(GameObject g)
    {
        ObjectToHit o = g.GetComponent<ObjectToHit>();
        GameObject tempObj = null;

        switch (o.isVeggie)
        {
            case true:
                score++;
                tempObj = SuccessParticle;
                break;

            case false:
                if (score > 0)
                    score--;
                mistakes++;

                if (mistakes == 3)
                {
                    Destroy(g);
                    ScareMe();
                    return;
                }

                tempObj = FailParticle;
                break;
        }
        Instantiate(tempObj, g.transform.position, Quaternion.identity);
        Destroy(g);
    }

    void ScareMe()
    {
        DestroyAll();
        //Handheld.Vibrate();
        //Handheld.PlayFullScreenMovie("LisaScare.mp4", Color.black, FullScreenMovieControlMode.Hidden);
        state = GAMESTATE.ENDGAME;
    }

    public void ResetGame()
    {
        mistakes = 0;
        time = maxTime;
        endTimer = 2.0f;
        score = 0;
        state = GAMESTATE.GAME;
    }

    void OnGUI()
    {
        GUI.skin = defaultSkin;
        GUI.skin.button.fontSize = Screen.height / 10;
        GUI.skin.label.fontSize = Screen.height / 10;
        switch (state)
        {
            case GAMESTATE.TITLE:
                GUI.Label(titleRect, titleScreen);

                if (GUI.Button(lowerLeftButton, "Start"))
                {
                    ResetGame();
                }
                if (GUI.Button(lowerRightButton, "How to Play"))
                    state = GAMESTATE.HOWTO;
                break;

            case GAMESTATE.GAME:
                if (showScore)
                {
                    GUI.Label(scoreRect, "Score: " + score.ToString());
                    GUI.Label(timeRect, "Time: " + ((int)time).ToString());
                }
                break;

            case GAMESTATE.ENDGAME:
                //if (mistakes >= 3)
                    //GUI.Label(fullScreenRect, scare);
                break;

            case GAMESTATE.GAMEOVER:
                if (mistakes >= 3)
                    GUI.Label(titleRect, "YOU ATE MY GOODIES!\n\n\n\nNo cake for you!");
                else
                    GUI.Label(titleRect, "YOU LOSE!\n\nYou only got " + score.ToString() + " points!\n\nNo cake for you!\nTry HARDER!");

                if (GUI.Button(lowerLeftButton, "Return to Title"))
                    state = GAMESTATE.TITLE;
                if (GUI.Button(lowerRightButton, "Retry"))
                    ResetGame();
                break;

            case GAMESTATE.WIN:
                GUI.Label(titleRect, "YOU WIN!\n\nYou got " + score.ToString() + " points!");
                if (GUI.Button(lowerMiddleButton, "Receive cake"))
                {
                    state = GAMESTATE.CAKE;
                    cake.SetActive(true);
                }
                break;

            case GAMESTATE.CAKE:
                GUI.Label(titleRect, "Happy Birthday Michael!");
                if (GUI.Button(lowerMiddleButton, "Return to Title"))
                {
                    cake.SetActive(false);
                    state = GAMESTATE.TITLE;
                }
                break;
        }
    }
}
